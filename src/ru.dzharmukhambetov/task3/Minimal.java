package ru.dzharmukhambetov.task3;

import java.util.Scanner;

public class Minimal {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println(" Введите первое число ");
        double a = scanner.nextDouble();
        System.out.println(" Введите второе число ");
        double b = scanner.nextDouble();
        scanner.close();
        double c;
        if (a < b) {
            c = a;
        } else if (a > b) {
            c = b;
        } else {
            c = a;
            System.out.println(" Эти числа равны ");

        }

        System.out.println(" наименьшее число " + c);

    }

}
