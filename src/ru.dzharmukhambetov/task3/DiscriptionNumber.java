package ru.dzharmukhambetov.task3;

import java.util.Scanner;

public class DiscriptionNumber {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println( " Введите число ");
        int a = scanner.nextInt();
        if (a>=0) {
            System.out.println(" Число " + a + " положительное ");
        }
        if (a<0) {
            System.out.println(" Число " + a + " отрицательное ");
        }
        int cel;
        cel=a%2;
        if (cel==0) System.out.println(" Число " + a + " Четное");
        if (cel!=0) System.out.println(" Число " + a + " Нечетное");
        }
}
