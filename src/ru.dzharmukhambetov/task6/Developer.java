package ru.dzharmukhambetov.task6;

public class Developer extends Person {

    private String developerName;
    private String developerLastName;

    public Developer (String name, String lastName){
        developerName = name;
        developerLastName = lastName;
    }
}
